variable "service" {
  description = "The name of the service, will be interpolated into the name at position 1"
  type = "string"
}

variable "zone" {
  description = "The zone name, will be interpolated into the name at position 2"
  type = "string"
}

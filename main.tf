resource "google_compute_address" "ip_address" {

  lifecycle {
    create_before_destroy = true
  }

  name = "${var.service}-${var.zone}"
}

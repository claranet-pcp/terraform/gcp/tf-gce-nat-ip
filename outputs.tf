output "name" {
  value = "${google_compute_address.ip_address.name}"
}
output "address" {
  value = "${google_compute_address.ip_address.address}"
}
output "link" {
  value = "${google_compute_address.ip_address.self_link}"
}
output "region" {
  value = "${google_compute_address.ip_address.region}"
}
